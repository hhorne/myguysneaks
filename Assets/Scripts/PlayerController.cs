using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
	[Header("Component References")]
	public new Rigidbody rigidbody;
	public Animator playerAnimator;

	[Header("Movement Settings")]
	public float movementSpeed = 2f;
	public float turnSpeed = 0.1f;
	[Range(0.0f, 0.3f)]
	public float RotationSmoothTime = 0.12f;

	float rotationSpeed;
	Vector2 movementDirection;
	bool isCrouching;
	bool isCrawling;
	bool isCovering;

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Wall")
		{
			var wallForward = collision.GetContact(0).normal;
			var playerForward = transform.forward;
			var playerBackward = playerForward * -1;
			var difference = Vector3.Angle(wallForward, playerBackward);
			isCovering = difference < 10f;
		}
		Debug.Log("toot toot!");
	}

	void OnUncover(InputValue value)
	{
		isCovering = false;
	}

	private void OnMove(InputValue value)
	{
		movementDirection = value.Get<Vector2>();
	}

	private void OnCrouch(InputValue value)
	{
		if (isCrawling)
		{
			isCrawling = false;
		}
		else
		{
			isCrouching = !isCrouching;
		}
	}

	private void OnCrawl(InputValue value)
	{
		if (isCrawling)
		{
			isCrawling = false;
			isCrouching = false;
		}
		else
		{
			isCrouching = true;
			isCrawling = true;
		}
	}

	private void Update()
	{
		playerAnimator.SetFloat("MoveX", movementDirection.x);
		playerAnimator.SetFloat("MoveY", movementDirection.y);
		playerAnimator.SetBool("IsCrouching", isCrouching);
		playerAnimator.SetBool("IsCrawling", isCrawling);
		playerAnimator.SetBool("IsCovering", isCovering);
	}

	private void FixedUpdate()
	{
		var camForward = GetCameraForward();
		var camRight = GetCameraRight();
		var m = new Vector3();
		m += movementDirection.x * camRight;
		m += movementDirection.y * camForward;

		MoveThePlayer(m);
		TurnThePlayer(m);
	}

	void AltMoveThePlayer(Vector3 direction)
	{
	}

	void AltTurnThePlayer(Vector3 direction)
	{
		Vector3 inputDirection = direction.normalized;
		var camTransform = Camera.main.transform;
		// note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
		// if there is a move input rotate player when the player is moving
		if (movementDirection != Vector2.zero)
		{
			var rotationVelocity = 0f;
			var targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + GetCameraForward().y;
			float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref rotationVelocity, RotationSmoothTime);

			// rotate to face input direction relative to camera position
			transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
		}
	}

	Vector3 GetCameraForward()
	{
		Vector3 forward = Camera.main.transform.forward;
		forward.y = 0f;
		return forward.normalized;

	}

	Vector3 GetCameraRight()
	{
		Vector3 right = Camera.main.transform.right;
		right.y = 0f;
		return right.normalized;
	}

	// Simplest movement
	void MoveThePlayer(Vector3 direction)
	{
		var d = direction * movementSpeed * Time.deltaTime;
		rigidbody.MovePosition(transform.position + d);
	}

	void TurnThePlayer(Vector3 direction)
	{
		if (movementDirection.sqrMagnitude > 0.01f)
		{
			Quaternion rotation = Quaternion.Slerp(
				rigidbody.rotation,
				Quaternion.LookRotation(direction),
				turnSpeed
			);

			rigidbody.MoveRotation(rotation);
		}
	}
}

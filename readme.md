# My Guy Sneaks

Having fun replicating sneaking / stealth mechanics from games like Metal Gear Solid.

## Requires

Unity 2020.3.18f LTS (Considering a jump to more recent non-LTS version to get access to newer features. Holding off because I don't know what features I'd even want and examples are more plentiful for older versions anyway.)

## Current Initiative

Movement Animation and Camera Tracking


## Resources

### Animations

[Animations via Adobe Mixamo](https://www.mixamo.com/#/)


### Videos

[Introduction to Input System](https://www.youtube.com/watch?v=YHC-6I_LSos)

[Making a 3rd Person Game with Unity Input and Camera](https://www.youtube.com/watch?v=WIl6ysorTE0)

[Root Motion Explained](https://www.youtube.com/watch?v=9bV0K-pifXE)


### Examples / Demo

#### Unity Step by Step intro to Input System

https://learn.unity.com/project/using-the-input-system-in-unity?uv=2020.1

#### InputSystem Warriors - Multicontroller

[Demo Sourcecode](https://github.com/unitytechnologies/inputsystem_warriors)

[Deepdive Talk](https://www.youtube.com/watch?v=xF2zUOfPyg8)